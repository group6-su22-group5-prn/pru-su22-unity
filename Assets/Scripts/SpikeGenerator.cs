﻿using System.Collections;
using System.Threading;
using UnityEngine;

public class SpikeGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject spike;
    [SerializeField]
    private float MinSpeed;
    [SerializeField]
    private float MaxSpeed;
    public float currentSpeed;
    [SerializeField]
    private float SpeedMultiplier;
    [SerializeField]
    PlayerScript player;

    // Start is called before the first frame update
    void Awake()
    {
        currentSpeed = MinSpeed;
        generateSpike();
    }

    public void GenerateNextSpikeDiffGap()
    {
        float randomWait = Random.Range(0.5f, 2f);
        Invoke("generateSpike", randomWait);
    }

    void generateSpike()
    {
        /*StartCoroutine(spawnAfterTime());*/
            GameObject SpikeIns = Instantiate(spike, transform.position, transform.rotation);
            SpikeIns.GetComponent<SpikeScript>().MySpikeGenerator = this;
        
    }

    IEnumerator spawnAfterTime()
    {
        if (spike.gameObject.CompareTag("air"))
        {
            yield return new WaitForSeconds(5);
            GameObject SpikeIns = Instantiate(spike, transform.position, transform.rotation);
            SpikeIns.GetComponent<SpikeScript>().MySpikeGenerator = this;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentSpeed < MaxSpeed)
        {
            currentSpeed += SpeedMultiplier;
        }
    /*    Debug.Log(player.MyScore);*/
    }
}
