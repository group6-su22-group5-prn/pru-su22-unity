using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public Text highscoretext;

    public float scoreCount;
    public float highscoreCount;

    public float pointPerSecond;

    public bool scoreIncreasing;


    private void Start()
    {
        if(PlayerPrefs.GetFloat("HighScore")!= null)
        {
            highscoreCount = PlayerPrefs.GetFloat("HighScore");
        }
    }
    void Update()
    {   
        
        if (scoreIncreasing)
        {
            scoreCount += pointPerSecond * Time.deltaTime;
        }
        if(scoreCount > highscoreCount)
        {
            highscoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", highscoreCount);
        }
        scoreText.text = "Score: " + Mathf.Round(scoreCount);
        highscoretext.text = "High Score: " + Mathf.Round(highscoreCount);
    }
    
}

