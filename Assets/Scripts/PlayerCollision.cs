using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private Rigidbody2D RB;
    private Animator myAnimator;
    public static event Action OnPlayerDeath;
    private void Awake()
    {
        RB = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("spike") || collision.gameObject.CompareTag("air"))
        {
            Die();
        }
    }
  
    private void Die()
    {   
        RB.bodyType = RigidbodyType2D.Static;
        myAnimator.SetTrigger("death");
     
    }

    
     private void RestartLevel()
     {
      FindObjectOfType<GameManager>().EndGame();
   }
}
