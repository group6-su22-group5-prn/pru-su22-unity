﻿using UnityEngine;

public class SpikeScript : MonoBehaviour
{
    [SerializeField]
    private SpikeGenerator spikeGenerator;
    public SpikeGenerator MySpikeGenerator
    {
        get { return spikeGenerator; }
        set { spikeGenerator = value; }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * spikeGenerator.currentSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("nextLine"))
        {
            spikeGenerator.GenerateNextSpikeDiffGap();  
        }

        if (collision.gameObject.CompareTag("Finish"))
        {
            Destroy(this.gameObject);
        }
    }
}
