﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
   
    [SerializeField]
    private float JumpForce;
    private float score;
    private float highscore;
    AudioSource playerjump;
    [SerializeField]
    private Text hightscoreText;

    private float CheckPoint;
    public float MyScore
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

    public float HighScore
    {
        get
        {
            return highscore;
        }
        set
        {
            highscore = value;
        }
    }

    private bool isGrounded = true;
    private bool isAlive = true;
    private Rigidbody2D RB;

    [SerializeField]
    private Text ScoreTxt;

    private Animator myAnimator;
    //Jump higher
    private float jumpTimeCounter;
    public float jumpTime;
    private bool isJumping;


    private void Awake()
    {
        RB = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();

        MyScore = 0;

        HighScore = PlayerPrefs.GetFloat("highscore");
        
         playerjump = GetComponent<AudioSource>();
    }

    private void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
        {
            isJumping = true;
            jumpTimeCounter = jumpTime;
            RB.AddForce(Vector2.up * JumpForce);
            isGrounded = false;
        }
        if (Input.GetKey(KeyCode.Space) && isJumping == true)
        {
            if (jumpTimeCounter > 0)
            {
                playerjump.Play();
                RB.AddForce(Vector2.up * JumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJumping = false;
            }
        }
        if (Input.GetKey(KeyCode.DownArrow) && isGrounded == false)
        {
            RB.transform.Translate(new Vector2(0f, 1f) * -JumpForce * Time.deltaTime);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }

        //HIGH SCORE
        Debug.Log("Diem cao :" + HighScore);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                RB.AddForce(Vector2.up * JumpForce);
                isGrounded = false;
            }
        }

        if (isAlive)
        {
            MyScore += Time.deltaTime;
          //  ScoreTxt.text = "SCORE : " + MyScore.ToString("N0");
           /* hightscoreText.text = "High Score : " + HighScore.ToString("N0");*/
        }
      
            PlayerPrefs.SetFloat("highscore", MyScore); 
 
 
        myAnimator.SetBool("Alive", isAlive);
        myAnimator.SetBool("Grounded", isGrounded);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            if (isGrounded == false)
            {
                isGrounded = true;
            }

        }
    }

}
