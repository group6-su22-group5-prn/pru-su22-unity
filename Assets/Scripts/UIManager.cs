using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject gameOverMenu;

   private ScoreManager thescoremanager;
     void Start()
    {
       thescoremanager = FindObjectOfType<ScoreManager>();
    }
    private void OnEnable()
    {
        GameManager.OnPlayerDeath += EnabledGameOverMenu;
    }
    private void OnDisable()
    {
        GameManager.OnPlayerDeath -= EnabledGameOverMenu;
    }
    public void EnabledGameOverMenu()
    {
        gameOverMenu.SetActive(true);
    }
    public void RestartLevel()
    {
        thescoremanager.scoreIncreasing = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        thescoremanager.scoreCount = 0;
        thescoremanager.scoreIncreasing = true; 
    }
    public void GotoMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
