using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;

    float restartDelay = 0f;
    public static event Action OnPlayerDeath;
    public void EndGame()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            // Invoke("Restart", restartDelay);
            OnPlayerDeath?.Invoke();
            Time.timeScale = 0;
        }
        
    }

   // void Restart()
  //  {
   //     SceneManager.LoadScene(SceneManager.GetActiveScene().name);
   // }
}
